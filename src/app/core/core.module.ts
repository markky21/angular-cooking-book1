import {NgModule} from '@angular/core';
import {NavComponent} from './nav/nav.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [SharedModule,],
  exports: [NavComponent],
  declarations: [NavComponent]
})
export class CoreModule {
}
