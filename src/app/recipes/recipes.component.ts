import {Component, OnInit} from '@angular/core';
import {Recipe} from '../shared/Recipe.model';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('Test recipe',
      'This is a simple test Recipe',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSnqLShVHdHIWTPCMI80qfp8_Pl1Np-uHUnkWASq09QiM3lphn'),
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
