import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {RecipesComponent} from './recipes.component';
import {RecipeListComponent} from './recipe-list/recipe-list.component';
import {RecipeDetailComponent} from './recipe-detail/recipe-detail.component';
import {RecipeItemComponent} from './recipe-item/recipe-item.component';

@NgModule({
  imports: [SharedModule],
  exports: [RecipesComponent],
  declarations: [RecipesComponent, RecipeListComponent, RecipeDetailComponent, RecipeItemComponent]
})
export class RecipesModule {
}
